package main

type HTTPMessage struct {
	Code int `json:"code"`
	ShortMessage string `json:"smsg"`
	Description string `json:"desc"`
}

var messages := map[int]HTTPMessage {
	404: HTTPMessage{Code: 404, ShortMessage: "Not Found", Description: path + "is not supported in this service"}
}
