package main

import ("io"
	"encoding/json")

type Strategy string

const (
   Honest Strategy = "honest"
   Corrupt = "corrupt"
   EagleEyes = "eagle-eyes"
)

var referees = map[Strategy]Decidable {
	Honest   : HonestReferee{},
	Corrupt  : CorruptReferee{},
	EagleEyes: EagleEyesReferee{},
}

func IsStrategy(s Strategy) bool {
	return true
}


type RefereeController struct {
	Referees []Referee
}

func (sc *RefereeController) Insert(body io.ReadCloser) (HTTPMessage) {
	var r Referee
	err := json.NewDecoder(body).Decode(&r)
	
	if err != nil {
		return HTTPMessage{Code: 500, ShortMessage: "I am broken!"}
	}
	
	// TODO: set UUID in referee
	sc.Referees = append(sc.Referees, r)
	return HTTPMessage{Code: 201, ShortMessage:"SR garantido"}
}

func (sc *RefereeController) SearchByName(name string) Referee {
	for i := 0; i < len(sc.Referees); i++ {
		if (name == sc.Referees[i].Name) {
			return sc.Referees[i]
		}
	}
	
	return Referee{}
}






















