package main

type CorruptReferee struct {
	Referee
}

func (CorruptReferee) Decide(event Event) Decision {
	return Decision{Infraction: "Cool", Interpretation: "Everything is Fine"}
}
