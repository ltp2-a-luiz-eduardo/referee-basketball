package main

type EagleEyesReferee struct {
	Referee
}

func (EagleEyesReferee) Decide(event Event) Decision {
	return Decision{Infraction: "Penalty", Interpretation: "I Saw It"}
}

func (EagleEyesReferee) String() Strategy {
	return EagleEyes
}
