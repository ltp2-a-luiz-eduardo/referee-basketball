package main

import ("log"
	"time"
	"net/http"
	"encoding/json")

var sc *RefereeController = &RefereeController{}
	
type Router struct {}

func (Router) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	url := req.URL
	
	path := url.Path
	
	if path != "/judge" {
		resp := messages[404]
		cebola, _ := json.Marshal(resp)
		res.Write(cebola)
		return
	}
	
	if path == "/judge" && req.Method == "POST" {
		resp := sc.Insert(req.Body)
		cebola, _ := json.Marshal(resp)
		res.Write(cebola)
		return
		
	}
	
	if path == "/judge" && req.Method == "GET" {
		name := url.Query().Get("name")
		referee := sc.SearchByName(name)
		cebola, _ := json.Marshal(referee)
		res.Write(cebola)
		return
	}
	
	
	
	s := url.Query().Get("strategy")
	decision := referees[Strategy(s)].Decide(Event{})
	
	decisionJson, _ := json.Marshal(decision)
	res.Write(decisionJson)
}

func RunService() {
	s := &http.Server{
		Addr:           "172.22.51.15:8080",
		Handler:        Router{},
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}
	
	log.Fatal(s.ListenAndServe())
}









