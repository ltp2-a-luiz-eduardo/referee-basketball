package main

type HonestReferee struct {
	Referee
}

func (HonestReferee) Decide(event Event) Decision {
	return Decision{Infraction: "Penalty", Interpretation: "Rules Violation"}
}
